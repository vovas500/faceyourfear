using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraManager : MonoBehaviour
{
    /// <summary>
    /// Get the instance of Game Play Manager
    /// </summary>
    public static CameraManager getInstance { get; private set; }

    /// <summary>
    /// Active player camera
    /// </summary>
    Camera playerCamera;
    /// <summary>
    /// Should we use smooth camea movement?
    /// </summary>
    [SerializeField] private bool useSmoothMovement;
    /// <summary>
    /// Smooth step for better visual slising on position change
    /// </summary>
    [SerializeField] private float smoothStep = 0.125f;
    /// <summary>
    /// Get the camera position
    /// </summary>
    /// <returns>Camera position</returns>
    [HideInInspector] public Vector3 GetCameraPosition() { return playerCamera.transform.position; }

    void Awake()
    {
        if (getInstance == null)
        {
            getInstance = this;
            DontDestroyOnLoad(this.gameObject);
        }
        else if (getInstance != this)
        {
            Destroy(this);
        }
    }

    // Start is called before the first frame update
    void Start()
    {
        playerCamera = this.gameObject.GetComponent<Camera>();
    }

    private void LateUpdate()
    {
        // Desired camera position (position to transit)
        Vector3 desiredPosition = GameplayManager.getInstance.GetPlayerPosition();
        Vector3 smoothPosition = Vector3.negativeInfinity;

        // Smooth our transition
        if (useSmoothMovement)
            smoothPosition = Vector3.Lerp(playerCamera.transform.position, desiredPosition, Time.deltaTime * smoothStep); // ok time offset is 5
        else
            smoothPosition = desiredPosition;

        playerCamera.transform.position = new Vector3
        (
            smoothPosition.x,
            smoothPosition.y,
            playerCamera.transform.position.z
        );
    }
}