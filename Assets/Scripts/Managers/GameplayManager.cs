using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using Enum = System.Enum;

public enum GamePlayState
{
    Play,
    Restart,
    Intro,
    Loose,
    Win,
    WinPost,
    PauseMenu,
    SettingsMenu,
    MainMenu,
}

public class GameplayManager : MonoBehaviour
{
    /// <summary>
    /// Get the instance of Game Play Manager
    /// </summary>
    public static GameplayManager getInstance { get; private set; }
    public static bool isPaused;
    public static GamePlayState gameState;

    [Header("Max Values")]
    [SerializeField]
    private float oxygenMax;
    [SerializeField]
    private float fearMax;

    [Header("Think Time Values")]
    [SerializeField]
    private float oxygenThinkTime;
    [SerializeField]
    private float fearThinkTime;
    [SerializeField]
    private float oxygenDecreaseValue;
    [SerializeField]
    private float fearIncreaseValue;
    [SerializeField]
    private float fearDecreaseValue;

    [Header("UI Objects")]
    [SerializeField]
    private GameObject pauseMenu;
    [SerializeField]
    private GameObject introMenu;
    [SerializeField]
    private GameObject winMenu;
    [SerializeField]
    private GameObject looseMenu;
    [SerializeField]
    private GameObject ingameUI;
    [SerializeField]
    private Image imageWin;
    [SerializeField]
    private Sprite postWinSprite;

    [Header("Audio")]
    [SerializeField]
    private AudioSource musicManager;

    bool allowToStart;

    // Elements values
    private float oxygenValue;
    private float fearValue;
    private float fearValueToAdd;
    private float deepValue;

    // Is we currently fear something?
    private bool isFear;

    // Fear reset time: next time when fear should be turned off
    private float fearResetTime;
    private float nextImageTime;
    private float nextImageTimeout = 4.0f;

    // Think timers
    private float fearNextThinkTime;
    private float oxygenNextThinkTime;
    private bool initialized = false;
    private WaterLevelType waterLevelType;
    

    // Player
    private Player player;
    private Monologues monologues;
    private float waterLevel;
    
    void Awake()
    {
        if (getInstance == null)
        {
            getInstance = this;
            DontDestroyOnLoad(this.gameObject);
        }
        else if (getInstance != this)
        {
            Destroy(this);
        }

        Initialize();
    }

    // Start is called before the first frame update
    void Start()
    {
        musicManager.volume = SettingsManager.getInstance.GetVolumeMaster() * SettingsManager.getInstance.GetVolumeMusic();

        ResetLevel();
    }

    /// <summary>
    /// ����� ��������
    /// </summary>
    private void ResetValues()
    {
        oxygenValue = oxygenMax; // � ������ ����������� ���������
        fearValue = 0; // � ������ ������ �� ������

        fearNextThinkTime = float.NegativeInfinity;
        oxygenNextThinkTime = float.NegativeInfinity;

        initialized = false;
    }

    /// <summary>
    /// ��������� ������
    /// </summary>
    public void ResetLevel()
    {
        ResetValues();
        Initialize();

        gameState = GamePlayState.Intro;
    }

    private void Initialize()
    {
        if (initialized)
            return;

        nextImageTime = float.PositiveInfinity;

        player = GameObject.FindObjectOfType<Player>();
        monologues = GameObject.FindObjectOfType<Monologues>();
        waterLevel = GameObject.FindObjectOfType<InfoWaterLevel>().GetWaterLevel();

        waterLevelType = WaterLevelType.AboveWater;

        gameState = GamePlayState.Intro;
    }

    /// <summary>
    /// ��������� ������ �������������
    /// </summary>
    /// <param name="value">������������ �������� ������</param>
    public void AddFearValue(float value)
    {
        fearValueToAdd += value;

        monologues.PrintScaryText();
    }

    /// <summary>
    /// ��������������� ��������
    /// </summary>
    public void RestoreOxygen()
    {
        oxygenValue = oxygenMax; // � ������ ����������� ���������
    }

    public Vector2 GetPlayerPosition()
    {
        return player.transform.position;
    }

    private void OxygenThink()
    {
        if (waterLevelType == WaterLevelType.AboveWater)
            return;

        if(Time.time > oxygenNextThinkTime)
        {
            oxygenNextThinkTime = Time.time + oxygenThinkTime;

            if (oxygenValue - oxygenDecreaseValue > 0)
                oxygenValue -= oxygenDecreaseValue;
            else
                oxygenValue = 0;

            if (oxygenValue <= 0)
                gameState = GamePlayState.Loose;
        }
    }

    public float GetOxygenValue()
    {
        return oxygenValue;
    }

    private void FearThink()
    {
        if (isFear)
        {
            if (Time.time > fearResetTime)
            {
                isFear = false;
            }

            if (Time.time > fearNextThinkTime)
            {
                // ���� ��� ���
                fearNextThinkTime = Time.time + fearThinkTime;

                if (fearValue + fearIncreaseValue > fearMax)
                    fearValue = fearMax;
                else
                    fearValue += fearIncreaseValue;

                Debug.Log($"Fear value: {fearValue}");
            }
        }
        else
        {
            fearValueToAdd = 0;
        }

        if (fearValue >= fearMax)
            gameState = GamePlayState.Loose;

        if (Time.time > fearNextThinkTime)
        {
            fearNextThinkTime = Time.time + fearThinkTime;

            if(fearValue > 0)
                fearValue -= fearDecreaseValue;

            Debug.Log($"Fear value: {fearValue}");
        }
    }

    public void ChangeWaterLevelType(int level)
    {
        waterLevelType = (WaterLevelType)Enum.Parse(typeof(WaterLevelType), level.ToString());
    }

    public float GetFearValue()
    {
        return fearValue;
    }

    public void Fear()
    {
        isFear = true;
        fearResetTime = Time.time + fearThinkTime;

        monologues.PrintHurtText();
    }

    private void DeepThink()
    {
        if (player.transform.position.y - waterLevel < 0)
            deepValue = Mathf.Abs(player.transform.position.y - waterLevel);
        else
            deepValue = 0;

        if (deepValue > 480)
            monologues.PrintCloseText();
    }

    public float GetDeepValue()
    {
        return deepValue;
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Escape) && (gameState == GamePlayState.Play || gameState == GamePlayState.PauseMenu))
        {
            if (gameState == GamePlayState.Play)
                //gameState = GamePlayState.MainMenu; // Da fuck happening in main menu???
                Application.Quit();
            /*else if (gameState == GamePlayState.PauseMenu || isPaused)
                gameState = GamePlayState.Play;*/
        }

        if (isPaused)
            return;

        if (Time.time > nextImageTime)
            gameState = GamePlayState.WinPost;

        switch (gameState)
        {
            case GamePlayState.Intro:
                Time.timeScale = 0;
                pauseMenu.SetActive(false);
                ingameUI.SetActive(false);
                // �������� ����� �����
                introMenu.SetActive(true);
                allowToStart = true;
                break;
            case GamePlayState.Play:
                Time.timeScale = 1;
                allowToStart = false;
                pauseMenu.SetActive(false);
                ingameUI.SetActive(true);
                DeepThink(); // ��������� �������: ��� �� ��� �������!
                OxygenThink(); // ��������� ��������: �� ������... ������������! �� �����... �������!
                FearThink(); // ��������� ������: ��������� ����� �����, ������!
                break;
            case GamePlayState.PauseMenu:
                Time.timeScale = 0;
                pauseMenu.SetActive(true);
                // ��������� �����? �� ���!
                break;
            case GamePlayState.Restart:
                ResetLevel();
                break;
            case GamePlayState.Loose:
                Time.timeScale = 0;
                allowToStart = true;
                pauseMenu.SetActive(false);
                ingameUI.SetActive(false);
                looseMenu.SetActive(true);
                // ����� ���������
                break;
            case GamePlayState.Win:
                nextImageTime = Time.time + nextImageTimeout;
                allowToStart = false;
                pauseMenu.SetActive(false);
                ingameUI.SetActive(false);
                winMenu.SetActive(true);
                // ����� ������
                break;
            case GamePlayState.WinPost:
                imageWin.sprite = postWinSprite;
                Time.timeScale = 0;
                allowToStart = true;
                break;
            case GamePlayState.MainMenu:
                Time.timeScale = 0;
                allowToStart = false;
                SceneManager.LoadSceneAsync("MainMenu", LoadSceneMode.Single);
                break;
        }

        if (allowToStart &&
            (gameState != GamePlayState.Play && gameState != GamePlayState.PauseMenu))
            StartGame();
    }

    private void StartGame()
    {
        if (Input.GetKeyDown(KeyCode.Space))
        {
            looseMenu.SetActive(false);
            winMenu.SetActive(false);
            introMenu.SetActive(false);

            switch (gameState)
            {
                case GamePlayState.Intro:
                    gameState = GamePlayState.Play;

                    break;
                case GamePlayState.Loose:
                case GamePlayState.WinPost:
                    gameState = GamePlayState.Restart;

                    break;
            }
        }
        else if (Input.GetKeyDown(KeyCode.Escape))
            gameState = GamePlayState.MainMenu;
    }

    public void WinGame()
    {
        Debug.Log("You win!");
        gameState = GamePlayState.Win;
    }

    public void OnApplicationPause(bool pause)
    {
        isPaused = pause;
    }

    private void OnApplicationFocus(bool focus)
    {
        isPaused = !focus;
    }
}