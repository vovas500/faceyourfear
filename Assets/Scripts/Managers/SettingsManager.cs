using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SettingsManager : MonoBehaviour
{
    /// <summary>
    /// Get the instance of Settings Manager
    /// </summary>
    public static SettingsManager getInstance { get; private set; }

    #region Volume settings
    /// <summary>
    /// Master sound volume
    /// </summary>
    [SerializeField] private float volumeMaster;
    /// <summary>
    /// SFX volume
    /// </summary>
    [SerializeField] private float volumeSFX;
    /// <summary>
    /// Music sound volume
    /// </summary>
    [SerializeField] private float volumeMusic;
    #endregion

    public void SetVolumeMaster(float volume) { volumeMaster = volume; }
    public void SetVolumeSFX(float volume) { volumeSFX = volume; }
    public void SetVolumeMusic(float volume) { volumeMusic = volume; }
    
    public float GetVolumeMaster() { return volumeMaster; }
    public float GetVolumeMusic() { return volumeMusic; }
    public float GetVolumeSFX() { return volumeSFX; }

    private void Awake()
    {
        if (getInstance == null)
        {
            getInstance = this;
            DontDestroyOnLoad(this.gameObject);
        }
        else if (getInstance != this)
        {
            Destroy(this);
        }

        LoadVolumeSettings();
    }

    private void Start()
    {
        LoadVolumeSettings();
    }

    public void LoadVolumeSettings()
    {
        volumeMaster = PlayerPrefs.GetFloat("volumeMaster", 1.0f);
        volumeMusic = PlayerPrefs.GetFloat("volumeMusic", 0.3f);
        volumeSFX = PlayerPrefs.GetFloat("volumeSFX", 0.3f);
    }

    public void SaveVolumeSettings()
    {
        PlayerPrefs.SetFloat("volumeMaster", volumeMaster);
        PlayerPrefs.SetFloat("volumeMusic", volumeMusic);
        PlayerPrefs.SetFloat("volumeSFX", volumeSFX);
        PlayerPrefs.Save();
    }

    public void ResetVolumeSettings()
    {
        volumeMaster = 1.0f;
        volumeMusic = 0.3f;
        volumeSFX = 0.3f;

        SaveVolumeSettings();
    }
}