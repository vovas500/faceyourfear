﻿using UnityEngine;
public class RandomExtended
{
    public static int RandomExclude(int min, int max, int exclude)
    {
        int result = Random.Range(min, max - 1);
        if (result >= exclude) result += 1;

        return result;
    }

    public static bool CalculateRandomChance(int chance)
    {
        if (chance == 0)
            return false;
        else if (chance == 100)
            return true;

        return Random.Range(0, 100) < chance ? true : false;
    }
}