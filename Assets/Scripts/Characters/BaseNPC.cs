using UnityEngine;

public class BaseNPC : BaseCharacter
{
    private bool shouldFearPlayer;
    private bool shouldBeStatic;
    private float fearRadius;

    private bool npcInitialized;
    
    private Vector2 defaultTargetPosition;
    private Vector2 currentTargetPosition;

    public void FishInit(float speed, Vector2 size, Vector2 defaulttarget, bool shouldfearplayer, float fearradius, bool staticnpc)
    {
        if (npcInitialized)
            return;

        //defaultTargetPosition = Vector2.negativeInfinity;
        //currentTargetPosition = Vector2.negativeInfinity;

        npcInitialized = true;

        swimSpeed = speed;
        spriteRnderer.size = size;
        boxCollider.size = size;
        shouldFearPlayer = shouldfearplayer;
        fearRadius = fearradius;
        shouldBeStatic = staticnpc;
        defaultTargetPosition = defaulttarget;
    }

    private void UpdateTarget()
    {
        if (shouldFearPlayer)
        {
            if(IsSeePlayer())
                currentTargetPosition = GameplayManager.getInstance.GetPlayerPosition();
        }
        else
        {
            currentTargetPosition = defaultTargetPosition;
        }
    }

    protected override void CharacterSwim()
    {
        if (shouldBeStatic)
            return; // �� ����� �������, �� ��������� ������� (aka ������ ���������� ���� ����!)

        // ���������� ���������
        Vector2 charactervelocity = characterRigedBody.velocity;

        charactervelocity += (Vector2)currentTargetPosition.normalized * swimSpeed * Time.deltaTime;

        characterRigedBody.velocity = charactervelocity;
    }

    private bool IsSeePlayer()
    {
        if(((Vector2)this.transform.position - GameplayManager.getInstance.GetPlayerPosition()).magnitude < 2)
        {
            return true;
        }

        return false;
    }

    private void UpdateFear()
    {
        if (shouldFearPlayer)
        {
            if (((Vector2)this.transform.position - GameplayManager.getInstance.GetPlayerPosition()).magnitude <= fearRadius)
            {
                //GameplayManager.getInstance.AddFearValue(1.0f);
                GameplayManager.getInstance.Fear();
            }
        }
    }

    // Update is called once per frame
    protected override void Update()
    {
        if (!npcInitialized)
            return;

        base.Update();

        UpdateTarget();
        UpdateFear();
    }

    protected override void FixedUpdate()
    {
        if (!npcInitialized)
            return;

        base.FixedUpdate();
    }
}