﻿namespace MinMaxSlider.Characters
{
    public enum FishType
    {
        Angler,
        Fish,
        Jelly,
        Moray,
        Octopus,
        Pirania,
        Puffer,
        SeaSnake,
        SeaHorse,
        Ghost,
    }
}