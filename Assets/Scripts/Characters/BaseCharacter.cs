using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BaseCharacter : MonoBehaviour
{
    [SerializeField]
    protected float swimSpeed = 1.0f;
    [SerializeField]
    protected float swimMaxSpeed = 5.0f;
    [SerializeField]
    protected float sprintMultiply = 2.0f;

    protected GameObject characterObject;
    protected CharacterFlag characterFlags;
    protected MoveDirectionH curMoveDirectionH;
    protected MoveDirectionH prevMoveDirectionH;
    protected MoveDirectionV curMoveDirectionV;
    protected MoveDirectionV prevMoveDirectionV;
    protected Rigidbody2D characterRigedBody;
    protected SpriteRenderer spriteRnderer;
    protected BoxCollider2D boxCollider;

    private bool initialized = false;
    protected Animator characterAnimator;

    // Start is called before the first frame update
    void Start()
    {
        Initialize();
    }

    void Awake()
    {
        characterObject = this.gameObject;

        Initialize();

        characterRigedBody = characterObject.GetComponent<Rigidbody2D>();
        characterAnimator = characterObject.GetComponent<Animator>();
        spriteRnderer = this.GetComponent<SpriteRenderer>();
        boxCollider = this.GetComponent<BoxCollider2D>();
    }
    
    private void Initialize()
    {
        if (initialized)
            return;

        curMoveDirectionH = MoveDirectionH.None;
        curMoveDirectionV = MoveDirectionV.None;
    }

    /// <summary>
    /// ����������� ���������
    /// </summary>
    protected virtual void CharacterSwim()
    {
        // ��������� ����� �� ������������ ��������� ���������
        float appliedswimmultiply = HasFlag(CharacterFlag.Sprint) ? sprintMultiply : 1.0f;

        // ���������� ���������
        Vector2 charactervelocity = characterRigedBody.velocity;

        if (curMoveDirectionH != MoveDirectionH.None)
        {
            // ���������
            charactervelocity.x += (int)curMoveDirectionH * swimSpeed;
        }
        else
        {
            // ��������� ��������
            if(charactervelocity.x > 0)
            {
                charactervelocity.x = charactervelocity.x - swimSpeed > 0 ? charactervelocity.x - swimSpeed * sprintMultiply : 0;
            }
            else
            {
                charactervelocity.x = charactervelocity.x + swimSpeed < 0 ? charactervelocity.x + swimSpeed * sprintMultiply : 0;
            }
        }
        
        if(curMoveDirectionV != MoveDirectionV.None)
        {
            // ���������
            charactervelocity.y += (int)curMoveDirectionV * swimSpeed;
        }
        else
        {
            // ��������� ��������
            if (charactervelocity.y > 0)
            {
                charactervelocity.y = charactervelocity.y - swimSpeed > 0 ? charactervelocity.y - swimSpeed * sprintMultiply : 0;
            }
            else
            {
                charactervelocity.y = charactervelocity.y + swimSpeed < 0 ? charactervelocity.y + swimSpeed * sprintMultiply : 0;
            }
        }

        characterRigedBody.velocity = new Vector2(
            Mathf.Clamp(charactervelocity.x, -swimMaxSpeed, swimMaxSpeed),
            Mathf.Clamp(charactervelocity.y, -swimMaxSpeed, swimMaxSpeed));
    }

    #region ������ � �������
    public void AddFlag(CharacterFlag flag)
    {
        characterFlags |= flag;
    }

    public void RemoveFlag(CharacterFlag flag)
    {
        characterFlags &= ~flag;
    }

    public bool HasFlag(CharacterFlag flag)
    {
        return (characterFlags & flag) != 0;
    }
    #endregion

    protected virtual void CharacterRotation()
    {
        float angle = Mathf.Atan2(characterRigedBody.velocity.y, characterRigedBody.velocity.x) * Mathf.Rad2Deg + 90.0f;
        transform.rotation = Quaternion.Lerp(transform.rotation, Quaternion.Euler(Vector3.forward * angle), swimSpeed);

        spriteRnderer.flipX = (angle >= -90 && angle < 0) || (angle > 180 && angle <= 270) ? true : false;
    }

    // Update is called once per frame
    protected virtual void Update()
    {
        if (GameplayManager.gameState != GamePlayState.Play)
            return;

        if (characterAnimator != null)
            characterAnimator.speed = (Mathf.Abs(characterRigedBody.velocity.x) + Mathf.Abs(characterRigedBody.velocity.y)) / 3;
    }

    protected virtual void FixedUpdate()
    {
        if (GameplayManager.gameState != GamePlayState.Play)
            return;

        CharacterSwim();
        CharacterRotation();
    }
}