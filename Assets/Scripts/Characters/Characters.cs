﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

/// <summary>
/// Перемещение по горизонтали
/// </summary>
public enum MoveDirectionH
{
    Left = -1,
    None,
    Right,
}

/// <summary>
/// Перемещение по вертикали
/// </summary>
public enum MoveDirectionV
{
    Down = -1,
    None,
    Up,
}

/// <summary>
/// Флаги для персонажей
/// </summary>
[Flags]
public enum CharacterFlag
{
    Sprint = 0,
    //MoveBackward = 0, // Backwar moving: special for intro & combat scenes
    //UseLegacyAnimator = 1, // Flag that allow to detect which animator version we should use
    //ControlledByAI = 2,
    //ForceControlledByAI = 4,
    //AllowToMove = 8,
    //FirstWalkingCycle = 16,
    //PlayTransitionAnimation = 32,
    //PlayDangerAnimation = 64,
    //AnimationsFlipped = 128,
}