using Unity.VisualScripting;
using UnityEngine;
using UnityEngine.Rendering.Universal;

public class Player : BaseCharacter
{
    [SerializeField] private Light2D flashlight;
    [SerializeField] private float maxLight = 1.0f;
    [SerializeField] private float minLight = 0.6f; // Unity Sucks On Step 1

    private void UpdateFlashlightSize()
    {
        // �����, ��� ��� ��� ����� - ����� ����: https://forum.unity.com/threads/is-it-possible-to-change-falloffintensity-from-script.729545/
        //flashlight.intensity = Mathf.PingPong(Time.deltaTime * maxLight * 25 + maxLight, maxLight * 10);
    }

    // Update is called once per frame
    protected override void Update()
    {
        if (GameplayManager.gameState != GamePlayState.Play)
            return;

        //UpdateFlashlightSize();

        base.Update();

        // ���������� �������� �����������
        if ((Input.GetKeyUp(KeyCode.W) || Input.GetKeyUp(KeyCode.UpArrow)) && prevMoveDirectionV == MoveDirectionV.Up)
            curMoveDirectionV = MoveDirectionV.None;
        else if ((Input.GetKeyUp(KeyCode.S) || Input.GetKeyUp(KeyCode.DownArrow)) && prevMoveDirectionV == MoveDirectionV.Down)
            curMoveDirectionV = MoveDirectionV.None;

        if ((Input.GetKeyUp(KeyCode.D) || Input.GetKeyUp(KeyCode.RightArrow)) && prevMoveDirectionH == MoveDirectionH.Right)
            curMoveDirectionH = MoveDirectionH.None;
        else if ((Input.GetKeyUp(KeyCode.A) || Input.GetKeyUp(KeyCode.LeftArrow)) && prevMoveDirectionH == MoveDirectionH.Left)
            curMoveDirectionH = MoveDirectionH.None;

        // ����������� ���������
        if (Input.GetKeyDown(KeyCode.W) || Input.GetKeyDown(KeyCode.UpArrow))
        {
            curMoveDirectionV = MoveDirectionV.Up;
            prevMoveDirectionV = curMoveDirectionV;
        }
        else if (Input.GetKeyDown(KeyCode.S) || Input.GetKeyDown(KeyCode.DownArrow))
        {
            curMoveDirectionV = MoveDirectionV.Down;
            prevMoveDirectionV = curMoveDirectionV;
        }

        if (Input.GetKeyDown(KeyCode.A) || Input.GetKeyDown(KeyCode.LeftArrow))
        {
            curMoveDirectionH = MoveDirectionH.Left;
            prevMoveDirectionH = curMoveDirectionH;
        }
        else if (Input.GetKeyDown(KeyCode.D) || Input.GetKeyDown(KeyCode.RightArrow))
        {
            curMoveDirectionH = MoveDirectionH.Right;
            prevMoveDirectionH = curMoveDirectionH;
        }

        if(Input.GetKeyDown(KeyCode.LeftShift) || Input.GetKeyDown(KeyCode.RightShift))
        {
            AddFlag(CharacterFlag.Sprint);
        }
        else
        {
            RemoveFlag(CharacterFlag.Sprint);
        }
    }

    protected override void CharacterRotation()
    {
        base.CharacterRotation();
    }
}