using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class Fishes : MonoBehaviour
{
    [SerializeField] private FishData[] fishDefinitions;

    [Header("Population controller configuration")]
    [SerializeField] private List<GameObject> prefabCharacterNPC;
    [SerializeField] private int maxCharactersAlive = 20;
    [SerializeField] private float spawnTimeOut = 1.0f;
    [SerializeField] private float removeTimeOut = 5.0f;
    [SerializeField] private Transform npcParent;

    private List<BaseNPC> _currentCharactersAlive;
    private List<SpawnZone> _characterSpawnPointsNearPlayer;

    private float _nextSpawnTime;
    private float _nextRemoveTime;
    private bool _initialized;

    public int GetMaxCharacters() { return maxCharactersAlive; }
    public int GetCharactersAlive() { return _currentCharactersAlive.Count; }

    private FishData GetRandomFishByDeep(float deep)
    {
        List<FishData> fishmatches = new List<FishData>();

        foreach(FishData fishdata in fishDefinitions)
        {
            if(deep >= fishdata.allowSpawnOnDeep.x && deep <= fishdata.allowSpawnOnDeep.y)
                fishmatches.Add(fishdata);
        }

        if(fishmatches.Count > 0)
        {
            return fishmatches.Random();
        }

        Debug.LogWarning($"!!! WARNING: No characted data was found for deep \"{deep} m.\"");
        return null;
    }

    private void Awake()
    {
        Initialize();
        CacheSpawnPoints();
    }

    public void Reset()
    {
        _initialized = false;
    }

    private void CacheSpawnPoints()
    {
        _characterSpawnPointsNearPlayer.Clear();

        foreach(SpawnZone spawnpoint in GameObject.FindObjectsOfType<SpawnZone>())
        {
            if(spawnpoint.IsActivatingByDeep())
            {
                // Near player
                _characterSpawnPointsNearPlayer.Add(spawnpoint);
            }
        }
    }

    private void SpawnCharacterNearPlayer()
    {
        SpawnZone spawnpointinfo = _characterSpawnPointsNearPlayer.Random();

        if (!spawnpointinfo.IsActive())
        {
            // Too far from player, reduce next spawn time and skip spawning!
            _nextSpawnTime = Time.time + spawnTimeOut / 4;
            return;
        }

        FishData fishdata = GetRandomFishByDeep(Mathf.Abs(spawnpointinfo.GetSpawnPosition().y));

        if (fishdata == null)
            return; // �� ���� ������� ����� ��� ������ ��������

        Vector2 fishSpawnPosition = spawnpointinfo.GetSpawnPosition();

        Vector2 destination = spawnpointinfo.GetTargetDestination(fishdata.allowedMoveDirections, fishSpawnPosition);
        GameObject character = Instantiate(prefabCharacterNPC.Random(), fishSpawnPosition, Quaternion.identity, npcParent);

        character.GetComponent<BaseNPC>().FishInit(fishdata.fishSpeed, fishdata.size, destination, fishdata.shouldFear, fishdata.fearRadius, fishdata.shouldStatic);
        character.GetComponent<SpriteRenderer>().sprite = fishdata.fishSprite;
        character.name = fishdata.fishSprite.name;

        _currentCharactersAlive.Add(character.GetComponent<BaseNPC>());
    }

    private void RemoveCharacter()
    {
        List<BaseNPC> removeList = new List<BaseNPC>();
        removeList.AddRange(_currentCharactersAlive.Where(fish => ((Vector2)fish.transform.position - GameplayManager.getInstance.GetPlayerPosition()).magnitude > 20));

        foreach(BaseNPC npc in removeList)
        {
            _currentCharactersAlive.Remove(npc);
            //Debug.Log($"NPC {npc.name} was removed!");
            Destroy(npc.gameObject);
        }
    }

    private void UpdateSpawning()
    {
        if(Time.time > _nextSpawnTime)
        {
            _nextSpawnTime = Time.time + spawnTimeOut;

            if (_currentCharactersAlive.Count < maxCharactersAlive)
            {
                SpawnCharacterNearPlayer();
            }
        }
    }

    private void UpdateRemoveQueue()
    {
        if(Time.time > _nextRemoveTime)
        {
            _nextRemoveTime = Time.time + removeTimeOut;

            if (_currentCharactersAlive.Count > 0)
                RemoveCharacter();
        }
    }

    private void Initialize()
    {
        if (_initialized)
            return;

        _initialized = true;

        _currentCharactersAlive = new List<BaseNPC>();
        _characterSpawnPointsNearPlayer = new List<SpawnZone>();

        _nextSpawnTime = float.NegativeInfinity;
        _nextRemoveTime = float.NegativeInfinity;
    }

    private void Update()
    {
        UpdateRemoveQueue();
        UpdateSpawning();
    }
}