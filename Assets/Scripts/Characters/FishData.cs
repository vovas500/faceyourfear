﻿using UnityEngine;
using MinMaxSlider;
using System.Collections.Generic;

[CreateAssetMenu(fileName = "New FishData", menuName = "Fish Data", order = 51)]
public class FishData : ScriptableObject
{
    /*[SerializeField]
    public FishType fishType;*/
    [MinMaxSlider(0, 600)]
    public Vector2 allowSpawnOnDeep;
    [SerializeField]
    public List<Direction> allowedMoveDirections;
    [SerializeField]
    public Sprite fishSprite;
    [SerializeField]
    public float fishSpeed;
    [SerializeField]
    public bool shouldFear;
    [SerializeField]
    public bool shouldStatic;
    [SerializeField]
    public float fearRadius;
    [SerializeField]
    public Vector2 size;
}