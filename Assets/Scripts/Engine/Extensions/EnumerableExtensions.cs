﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

public static class EnumerableExtensions
{
    public static T Random<T>(this IEnumerable<T> enumerable)
    {
        var r = new Random();
        var list = enumerable as IList<T> ?? enumerable.ToList();
        return list.ElementAt(r.Next(0, list.Count()));
    }

    public static T RandomExclude<T>(this IEnumerable<T> enumerable, int min, int max, int exclude)
    {
        var list = enumerable as IList<T> ?? enumerable.ToList();
        return list.ElementAt(RandomExtended.RandomExclude(min, max, exclude));
    }
}