using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class Monologues : MonoBehaviour
{
    [Header("Components")]
    [SerializeField]
    GameObject monologueObject;
    [SerializeField]
    TextMeshProUGUI textMesh;

    [Header("Delay Values")]
    [SerializeField]
    float hurtDelayTime;
    [SerializeField]
    float scaryDelayTime;
    [SerializeField]
    float showTextDuration;

    [Header("Text templates")]
    [SerializeField]
    string[] fishesText;
    [SerializeField]
    string[] hurtText;
    [SerializeField]
    string[] scaryText;
    [SerializeField]
    string[] darkText;
    [SerializeField]
    string[] closeText;
    [SerializeField]
    string[] finalText;

    private bool fishesSaid;
    private bool closeSaid;
    private bool isInitialized;

    private float nextTimeHurt;
    private float nextTimeScary;
    private float nextTimeHideText;

    private void Awake()
    {
        Initialize();
    }

    private void Initialize()
    {
        if (isInitialized)
            return;

        nextTimeHurt = float.NegativeInfinity;
        nextTimeScary = float.NegativeInfinity;

        monologueObject.SetActive(false);
        isInitialized = true;
    }

    private void ShowText(string text)
    {
        nextTimeHideText = Time.time + showTextDuration;

        textMesh.text = text;
        monologueObject.SetActive(true);
    }

    public void PrintFishesText()
    {
        if(!fishesSaid)
        {
            fishesSaid = true;
            ShowText(fishesText[0]);
        }
    }

    public void PrintHurtText()
    {
        if (Time.time > nextTimeHurt)
        {
            nextTimeHurt = Time.time + hurtDelayTime;

            ShowText(hurtText[0]);
        }
    }

    public void PrintScaryText()
    {
        if(Time.time > nextTimeScary)
        {
            nextTimeScary = Time.time + scaryDelayTime;

            ShowText(scaryText[0]);
        }
    }

    public void PrintDarkText()
    {
        ShowText(darkText[0]);
    }

    public void PrintCloseText()
    {
        if (!closeSaid)
        {
            closeSaid = true;
            ShowText(closeText[0]);
        }
    }

    public void PrintFinalText()
    {
        ShowText(finalText[0]);
    }

    // Update is called once per frame
    void Update()
    {
        if(Time.time > nextTimeHideText)
        {
            monologueObject.SetActive(false);
        }
    }
}