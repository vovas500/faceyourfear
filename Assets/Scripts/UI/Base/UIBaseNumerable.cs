using TMPro;
using UnityEngine;

public class UIBaseNumerable : UIBase
{
    [SerializeField]
    private string textTemplatePre;
    [SerializeField]
    private string textTemplatePost;

    private TextMeshProUGUI textElement;

    // Start is called before the first frame update
    void Awake()
    {
        textElement = this.GetComponentInChildren<TextMeshProUGUI>();
    }

    // Update is called once per frame
    void Update()
    {
        string formatvalue = GetElementValue().ToString("000.0");
        textElement.text = $"{textTemplatePre} {formatvalue} {textTemplatePost}";
    }
}