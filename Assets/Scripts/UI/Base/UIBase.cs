using UnityEngine;

public class UIBase : MonoBehaviour
{
    protected virtual float GetElementValue()
    {
        Debug.LogError($"GetElementValue wasn't ovverrided for UI Element {this.gameObject.name}");
        return 0.0f;
    }
}