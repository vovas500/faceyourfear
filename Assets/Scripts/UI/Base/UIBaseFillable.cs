using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UIBaseFillable : UIBase
{
    [SerializeField]
    protected Image elementValue;
    [SerializeField]
    protected float maxSize;

    // Start is called before the first frame update
    void Awake()
    {
        if (elementValue == null)
            Debug.LogError($"SpriteRenderer wasn't set for UI Element {this.gameObject.name}");
    }

    // Update is called once per frame
    void Update()
    {
        elementValue.rectTransform.sizeDelta = new Vector2(GetElementValue() * maxSize / 100, elementValue.rectTransform.sizeDelta.y);
    }
}