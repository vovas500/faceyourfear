using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SettingsMenu : MonoBehaviour
{
    [SerializeField]
    Slider masterVolumeSlider;
    [SerializeField]
    Slider musicVolumeSlider;
    [SerializeField]
    Slider sfxVolumeSlider;

    // Start is called before the first frame update
    void Awake()
    {
        masterVolumeSlider.value = SettingsManager.getInstance.GetVolumeMaster();
        musicVolumeSlider.value = SettingsManager.getInstance.GetVolumeMusic();
        sfxVolumeSlider.value = SettingsManager.getInstance.GetVolumeSFX();
    }

    public void Apply()
    {
        SettingsManager.getInstance.SetVolumeMaster(masterVolumeSlider.value);
        SettingsManager.getInstance.SetVolumeMusic(musicVolumeSlider.value);
        SettingsManager.getInstance.SetVolumeSFX(sfxVolumeSlider.value);

        GameplayManager.gameState = GamePlayState.PauseMenu;
    }

    public void Default()
    {
        SettingsManager.getInstance.ResetVolumeSettings();

        masterVolumeSlider.value = SettingsManager.getInstance.GetVolumeMaster();
        musicVolumeSlider.value = SettingsManager.getInstance.GetVolumeMusic();
        sfxVolumeSlider.value = SettingsManager.getInstance.GetVolumeSFX();
    }

    public void Cancel()
    {
        GameplayManager.gameState = GamePlayState.PauseMenu;
    }
}