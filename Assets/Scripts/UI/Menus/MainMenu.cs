using UnityEngine;
using UnityEngine.SceneManagement;

public class MainMenu : MonoBehaviour
{
    /*[SerializeField]
    GameObject mainMenuObject;*/
    /*[SerializeField]*/
    GameObject settingsObject;
    [SerializeField]
    AudioSource ausioSourceMenu;

    bool initialized;
    bool isPlayPressed;

    private void Awake()
    {
        if (initialized)
            return;

        initialized = true;

        /*settingsObject = GameObject.FindObjectOfType<SettingsMainMenu>().gameObject;
        settingsObject.SetActive(false);*/
    }

    public void Play()
    {
        if (isPlayPressed)
            return;

        isPlayPressed = true; // Hack! To prevent multi-scene loading!

        ausioSourceMenu.Stop();
        SceneManager.LoadSceneAsync("GameMain", LoadSceneMode.Single);
        //SceneManager.UnloadSceneAsync("MainMenu", UnloadSceneOptions.UnloadAllEmbeddedSceneObjects);
    }

    public void Quit()
    {
        Application.Quit();
    }

    public void Settings()
    {
        this.gameObject.SetActive(false);
        settingsObject.SetActive(true);
    }
}