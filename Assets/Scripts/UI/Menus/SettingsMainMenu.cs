using UnityEngine;
using UnityEngine.UI;

public class SettingsMainMenu : MonoBehaviour
{
    //[SerializeField]
    GameObject mainMenuObject;

    [SerializeField]
    Slider masterVolumeSlider;
    [SerializeField]
    Slider musicVolumeSlider;
    [SerializeField]
    Slider sfxVolumeSlider;

    bool initialized;

    // Start is called before the first frame update
    void Awake()
    {
        masterVolumeSlider.value = SettingsManager.getInstance.GetVolumeMaster();
        musicVolumeSlider.value = SettingsManager.getInstance.GetVolumeMusic();
        sfxVolumeSlider.value = SettingsManager.getInstance.GetVolumeSFX();

        if (initialized)
            return;

        initialized = true;

        mainMenuObject = GameObject.FindObjectOfType<MainMenu>().gameObject;
    }

    public void Apply()
    {
        SettingsManager.getInstance.SetVolumeMaster(masterVolumeSlider.value);
        SettingsManager.getInstance.SetVolumeMusic(musicVolumeSlider.value);
        SettingsManager.getInstance.SetVolumeSFX(sfxVolumeSlider.value);

        SettingsManager.getInstance.SaveVolumeSettings();

        this.gameObject.SetActive(false);
        mainMenuObject.SetActive(true);
    }

    public void Default()
    {
        SettingsManager.getInstance.ResetVolumeSettings();

        masterVolumeSlider.value = SettingsManager.getInstance.GetVolumeMaster();
        musicVolumeSlider.value = SettingsManager.getInstance.GetVolumeMusic();
        sfxVolumeSlider.value = SettingsManager.getInstance.GetVolumeSFX();
    }

    public void Cancel()
    {
        this.gameObject.SetActive(false);
        mainMenuObject.SetActive(true);
    }
}