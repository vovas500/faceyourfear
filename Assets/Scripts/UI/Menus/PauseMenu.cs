using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PauseMenu : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        
    }

    public void ContinueButtonClick()
    {
        GameplayManager.gameState = GamePlayState.Play;
    }

    public void RestartButtonClick()
    {
        GameplayManager.gameState = GamePlayState.Restart;
    }

    public void SettingsButtonClick()
    {
        GameplayManager.gameState = GamePlayState.SettingsMenu;
    }

    public void MainMenuButtonClick()
    {
        GameplayManager.gameState = GamePlayState.MainMenu;
    }

    public void CancelButtonClick()
    {
        GameplayManager.gameState = GamePlayState.PauseMenu;
    }

    public void ApplyButtonClick()
    {
        GameplayManager.gameState = GamePlayState.PauseMenu;
    }

    public void DefaultButtonClick()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if (GameplayManager.gameState != GamePlayState.PauseMenu)
            this.gameObject.SetActive(false);
        else
            this.gameObject.SetActive(true);
    }
}