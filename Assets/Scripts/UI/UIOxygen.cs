using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UIOxygen : UIBaseFillable
{
    protected override float GetElementValue()
    {
        return GameplayManager.getInstance.GetOxygenValue();
    }
}