using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Math = System.Math;

public class UIDeep : UIBaseNumerable
{
    protected override float GetElementValue()
    {
        return (float)Math.Round((double)GameplayManager.getInstance.GetDeepValue(), 1) + 0.5f;
    }
}