using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UIFear : UIBaseFillable
{
    protected override float GetElementValue()
    {
        return GameplayManager.getInstance.GetFearValue();
    }
}