using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public enum TriggerType
{
    Once, // Action can be fired only once
    Multiple, // Action can be fired multiple times
    Touchable, // Need to stay in trigger for some time to fire the action
}

public class Trigger : MonoBehaviour
{
    [Header("Actions")]
    [SerializeField] public UnityEvent triggerEnteredAction;
    [SerializeField] public UnityEvent triggerExitAction;

    [Header("Trigger Options")]
    [SerializeField] private TriggerType triggerType;
    [SerializeField] private List<string> tagsMask;

    private bool isTriggered;

    // Start is called before the first frame update
    void Start()
    {
        isTriggered = false;
    }

    protected virtual void OnTriggerEnter2D(Collider2D collision)
    {
        if (tagsMask.Contains(collision.tag))
        {
            if (triggerType == TriggerType.Once && isTriggered)
                return;

            isTriggered = true;
            triggerEnteredAction.Invoke();

            if (triggerType == TriggerType.Once)
                Destroy(this.gameObject);
        }
    }

    protected virtual void OnTriggerExit2D(Collider2D collision)
    {
        if (tagsMask.Contains(collision.tag))
        {
            if (triggerType == TriggerType.Multiple || triggerType == TriggerType.Touchable)
                isTriggered = false;

            triggerExitAction.Invoke();
            //Debug.Log("TRIGGERED!");
        }
    }

    // Update is called once per frame
    void Update()
    {

    }
}
