using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class VolumeLimiter : MonoBehaviour
{
    enum DefinedSizeState
    {
        BoxCollider, // Use Box Collider Size
        RectTransform, // Use GameObject Size
        Custom, // Custom defined
    }

    [Header("Defined Size Type")]
    [SerializeField]
    private DefinedSizeState type = DefinedSizeState.Custom;

    [Header("Custom Padding Setup")]
    /// <summary>
    /// Left padding limit
    /// </summary>
    [SerializeField]
    private float leftLimit;
    /// <summary>
    /// Right padding limit
    /// </summary>
    [SerializeField]
    private float rightLimit;
    /// <summary>
    /// Bottom padding limit
    /// </summary>
    [SerializeField]
    private float bottomLimit;
    /// <summary>
    /// Top padding limit
    /// </summary>
    [SerializeField]
    private float topLimit;

    [Header("Additional Options")]
    /// <summary>
    /// Lines color in editor
    /// </summary>
    [SerializeField]
    protected Color32 color = Color.red;
    /// <summary>
    /// Should be limiter drawn in Unity Editor?
    /// </summary>
    [SerializeField]
    private bool drawInEditor = true;

    private BoxCollider2D boxCollider;
    private RectTransform rectTransform;

    public float GetLeftLimit()
    {
        switch (type)
        {
            case DefinedSizeState.BoxCollider:
                if (boxCollider != null)
                    return this.transform.position.x + boxCollider.offset.x - boxCollider.size.x / 2;

                break;
            case DefinedSizeState.RectTransform:
                if (rectTransform != null)
                    return this.transform.position.x + rectTransform.rect.x - rectTransform.rect.width / 2;

                break;
            default:
                return this.transform.position.x + leftLimit;
        }

        return 0;
    }

    public float GetRightLimit()
    {
        switch (type)
        {
            case DefinedSizeState.BoxCollider:
                if (boxCollider != null)
                    return this.transform.position.x + boxCollider.offset.x + boxCollider.size.x / 2;

                break;
            case DefinedSizeState.RectTransform:
                if (rectTransform != null)
                    return this.transform.position.x + rectTransform.rect.x + rectTransform.rect.width / 2;

                break;
            default:
                return this.transform.position.x + rightLimit;
        }

        return 0;
    }

    public float GetBottomLimit()
    {
        switch (type)
        {
            case DefinedSizeState.BoxCollider:
                if (boxCollider != null)
                    return this.transform.position.y + boxCollider.offset.y - boxCollider.size.y / 2;

                break;
            case DefinedSizeState.RectTransform:
                if (rectTransform != null)
                    return this.transform.position.y + rectTransform.rect.y - rectTransform.rect.height / 2;

                break;
            default:
                return this.transform.position.y + bottomLimit;
        }

        return 0;
    }

    public float GetTopLimit()
    {
        switch (type)
        {
            case DefinedSizeState.BoxCollider:
                if (boxCollider != null)
                    return this.transform.position.y + boxCollider.offset.y + boxCollider.size.y / 2;

                break;
            case DefinedSizeState.RectTransform:
                if (rectTransform != null)
                    return this.transform.position.y + rectTransform.rect.y + rectTransform.rect.height / 2;

                break;
            default:
                return this.transform.position.y + topLimit;
        }

        return 0;
    }

    // Update that works only in Unity Editor mode
    private void OnDrawGizmos()
    {
        boxCollider = GetComponent<BoxCollider2D>();
        rectTransform = GetComponent<RectTransform>();

        // Draw camera boundary lines only if we use limits
        if (drawInEditor)
        {
            // draw box around the camera boundary
            Gizmos.color = color;
            // Top line
            Gizmos.DrawLine(new Vector2(GetLeftLimit(), GetTopLimit()), new Vector2(GetRightLimit(), GetTopLimit()));
            // Right line
            Gizmos.DrawLine(new Vector2(GetRightLimit(), GetTopLimit()), new Vector2(GetRightLimit(), GetBottomLimit()));
            // Bottom line
            Gizmos.DrawLine(new Vector2(GetRightLimit(), GetBottomLimit()), new Vector2(GetLeftLimit(), GetBottomLimit()));
            // Left line
            Gizmos.DrawLine(new Vector2(GetLeftLimit(), GetBottomLimit()), new Vector2(GetLeftLimit(), GetTopLimit()));
        }
    }
}