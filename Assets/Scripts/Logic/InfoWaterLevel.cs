using UnityEngine;

public enum WaterLevelType
{
    AboveWater,
    WaterLight,
    WaterMedium,
    WaterDark,
}

public class InfoWaterLevel : MonoBehaviour
{
    public float GetWaterLevel()
    {
        return transform.position.y;
    }
}