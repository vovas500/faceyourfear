using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum Direction
{
    None,
    Up,
    Down,
    Left,
    Right,
}

public class SpawnZone : VolumeLimiter
{
    [SerializeField]
    private bool randomSpawnPositionInVolume;
    [SerializeField]
    private Direction spawnDirection;
    [SerializeField]
    private bool shouldBeActiveByDeep = false;
    [SerializeField]
    private float deepActivationValue;

    private bool isSpawnActive;

    private void Awake()
    {
        isSpawnActive = !shouldBeActiveByDeep;
    }

    public Vector3 GetSpawnPosition()
    {
        if (randomSpawnPositionInVolume)
            return new Vector3(Random.Range(GetLeftLimit(), GetRightLimit()), Random.Range(GetTopLimit(), GetRightLimit()), 0);

        return new Vector3(this.transform.position.x, this.transform.position.y, 0);
    }

    public Vector2 GetRandomTargetDestination()
    {
        Vector2 vecDirection = Vector2.negativeInfinity;

        switch(spawnDirection)
        {
            case Direction.Left:
                vecDirection = new Vector2(-537.0f, Random.Range(-20.0f, -600.0f));
                break;
            case Direction.Right:
                vecDirection = new Vector2(537.0f, Random.Range(-20.0f, -600.0f));
                break;
            case Direction.Up:
                vecDirection = new Vector2(Random.Range(-537.0f, 537.0f), -20.0f);
                break;
            case Direction.Down:
                vecDirection = new Vector2(Random.Range(-537.0f, 537.0f), -600.0f);
                break;
        }

        return vecDirection;
    }

    public Vector2 GetTargetDestination(List<Direction> alloweddirections, Vector2 entityposition)
    {
        Direction targetdirection;
        Vector2 vecDirection = Vector2.negativeInfinity;

        if (alloweddirections.Contains(spawnDirection))
        {
            targetdirection = spawnDirection;
        }
        else
        {
            targetdirection = alloweddirections.Random();
        }

        switch (targetdirection)
        {
            case Direction.Left:
                vecDirection = new Vector2(-537.0f, entityposition.y);
                break;
            case Direction.Right:
                vecDirection = new Vector2(537.0f, entityposition.y);
                break;
            case Direction.Up:
                vecDirection = new Vector2(entityposition.x, -20.0f);
                break;
            case Direction.Down:
                vecDirection = new Vector2(entityposition.x, -600.0f);
                break;
        }

        return vecDirection;
    }

    public bool IsActive()
    {
        return isSpawnActive;
    }

    public bool IsActivatingByDeep()
    {
        return shouldBeActiveByDeep;
    }

    private void Update()
    {
        if (shouldBeActiveByDeep)
        {
            if (GameplayManager.getInstance.GetDeepValue() >= deepActivationValue)
            {
                isSpawnActive = true;
            }
            else
            {
                isSpawnActive = false;
            }
        }
    }
}